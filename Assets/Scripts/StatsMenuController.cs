﻿using Mono.Data.Sqlite;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StatsMenuController : MonoBehaviour
{
    public Transform battlesPanel;
    public GameObject battleListingPrefab;
    public TextMeshProUGUI statsNick;
    public TextMeshProUGUI statsLevel;
    public TextMeshProUGUI statsWon;

    private string connectionString;   

    private void OnEnable()
    {
        connectionString = "Data Source=" + Application.persistentDataPath + "/statsDatabase.db";
        ClearBattlesListings();
        GetBattles();
        GetPlayerStats();
    }

    void ClearBattlesListings()
    {
        for (int i = battlesPanel.childCount - 1; i >= 0; i--)
        {
            Destroy(battlesPanel.GetChild(i).gameObject);
        }
    }

    void GetBattles()
    {
        string commandText = "SELECT * FROM Battles ORDER BY id DESC LIMIT 5";
        using (SqliteDataReader reader = DatabaseManager.Instance.ExecuteReader(connectionString, commandText))
        {
            while (reader.Read())
            {
                GameObject tempListing = Instantiate(battleListingPrefab, battlesPanel);
                tempListing.transform.GetChild(0).GetComponent<Text>().text = reader.GetString(1);
                tempListing.transform.GetChild(2).GetComponent<Text>().text = reader.GetString(2);
                if (reader.GetInt32(3) == 0)
                {
                    tempListing.transform.GetChild(1).GetComponent<Text>().text = "LOSS";
                }
                else if (reader.GetInt32(3) == 1)
                {
                    tempListing.transform.GetChild(1).GetComponent<Text>().text = "WIN";
                }
            }
        }
    }

    void GetPlayerStats()
    {
        string commandText = "SELECT nickname, played, won, xp FROM Player";
        using (SqliteDataReader reader = DatabaseManager.Instance.ExecuteReader(connectionString, commandText))
        {
            while (reader.Read())
            {
                statsNick.text = reader.GetString(0);
                statsLevel.text = "level " + (reader.GetInt32(3) / 50 + 1).ToString();
                statsWon.text = "won: " + ((int)((reader.GetFloat(2) / reader.GetFloat(1)) * 100)).ToString() + "%";
            }
        }
    }
}
