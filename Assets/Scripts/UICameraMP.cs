﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICameraMP : MonoBehaviour
{
    public Camera cam1, cam2;

    private PhotonView PV;

    void Start()
    {

        PV = GetComponent<PhotonView>();
        if (PV.IsMine)
        {
            CameraRotate();
        }
    }

    private void CameraRotate()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            Instantiate(cam1, new Vector3(0, 0, -10), Quaternion.Euler(0, 0, 0));
        }
        else
        {
            Instantiate(cam2, new Vector3(0, 0, -10), Quaternion.Euler(0, 0, 180));
        }
    }
}
