﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    public GameObject shot;
    public Transform shotOrigin;
    public float fireRate;

    private float nextFire;

    private void FixedUpdate()
    {
        if (Input.touchCount > 0 && Time.time > nextFire)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                Vector3 touchPosition = Camera.main.ScreenToWorldPoint(Input.touches[i].position);
                if (touchPosition.x > -1.5 && touchPosition.x < 1.5)
                {
                    Fire();
                }
            }
        }
    }

    void Fire()
    {
        nextFire = Time.time + fireRate;
        GameObject bullet = Instantiate(shot, shotOrigin.position, shotOrigin.rotation);
        bullet.GetComponent<Bullet>().Shooter = this.gameObject;
    }
}
