﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CharacterText : MonoBehaviour
{
    private TextMeshProUGUI textMesh;

    // Start is called before the first frame update
    void Start()
    {
        textMesh = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        textMesh.text = PlayerInfo.PI.allCharacters[PlayerInfo.PI.mySelectedCharacter].name.ToUpper();
    }
}
