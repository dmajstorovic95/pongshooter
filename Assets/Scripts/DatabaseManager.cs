﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mono.Data.Sqlite;
using System.Data;
using System.IO;
using UnityEngine.SceneManagement;

public class DatabaseManager : MonoBehaviour
{
    public static DatabaseManager Instance;
    
    public string Nickname { get => nickname; set => nickname = value; }
    public string Id { get => id; set => id = value; }

    private string dbPath;
    private string nickname;
    private string id;


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            if (Instance != this)
            {
                Destroy(gameObject);
                Instance = this;
            }
        }
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        Debug.Log(Application.persistentDataPath);
        dbPath = "Data Source=" + Application.persistentDataPath + "/statsDatabase.db";
        CreateTables();
    }

    public void CreateTables()
    {
        using (var dbConnection = new SqliteConnection(dbPath))
        {
            dbConnection.Open();
            using (var command = dbConnection.CreateCommand())
            {
                string query = "CREATE TABLE IF NOT EXISTS 'Player' ( " +
                                  "  'id' TEXT PRIMARY KEY, " +
                                  "  'nickname' TEXT NOT NULL, " +
                                  "  'played' INTEGER NOT NULL DEFAULT 0," +
                                  "  'won' INTEGER NOT NULL DEFAULT 0," +
                                  "  'xp' INTEGER NOT NULL DEFAULT 0" +
                                  ");";
                query += "CREATE TABLE IF NOT EXISTS 'Battles' (" +
                            " 'id' INTEGER PRIMARY KEY AUTOINCREMENT," +
                            " 'opponent' TEXT NOT NULL," +
                            " 'datetime' TEXT NOT NULL," +
                            " 'outcome' INTEGER NOT NULL" +
                            ");";
                command.CommandType = CommandType.Text;
                command.CommandText = query;

                var result = command.ExecuteNonQuery();
                Debug.Log("Create result: " + result);
            }
        }
    }

    public void CreateUser(string connectionString)
    {
        //Debug.Log("Uslo u create");

        if (string.IsNullOrWhiteSpace(Nickname)){
            Nickname = "Player " + Random.Range(0, 1000);
        }
        Id = GenerateIdString(8);

        //Debug.Log("Uslo u create");
        Debug.Log(connectionString);

        using (var dbConnection = new SqliteConnection(connectionString))
        {
            dbConnection.Open();
            using (var command = dbConnection.CreateCommand())
            {
                command.CommandText = "INSERT INTO Player (id, nickname) VALUES (@Id, @Nickname)";
                command.Parameters.Add(new SqliteParameter
                {
                    ParameterName = "Nickname",
                    Value = Nickname
                });
                command.Parameters.Add(new SqliteParameter
                {
                    ParameterName = "Id",
                    Value = Id
                });
                var result = command.ExecuteNonQuery();
                Debug.Log(result);
            }
        }
        PlayerPrefs.SetString("Nickname", Nickname);
        PlayerPrefs.SetString("Id", Id);
        Debug.Log(PlayerPrefs.GetString("Nickname"));
    }

    public void UpdateUser(int outcome, int xp, string opponent, string connectionString, string datetime)
    {
        using (var dbConnection = new SqliteConnection(connectionString))
        {
            dbConnection.Open();
            using (var command = dbConnection.CreateCommand())
            {
                string query = "UPDATE Player SET played = played + 1," +
                                                         "won = won + (@Outcome)," +
                                                         "xp = xp + (@Xp)" +
                                      "WHERE (nickname) = (@Nickname);";
                query += "INSERT INTO Battles (opponent, datetime, outcome) VALUES (@Opponent, @Datetime, @Outcome);";
                command.CommandText = query;
                command.Parameters.Add(new SqliteParameter
                {
                    ParameterName = "Outcome",
                    Value = outcome
                });
                command.Parameters.Add(new SqliteParameter
                {
                    ParameterName = "Xp",
                    Value = xp
                });
                command.Parameters.Add(new SqliteParameter
                {
                    ParameterName = "Nickname",
                    Value = Nickname
                });
                command.Parameters.Add(new SqliteParameter
                {
                    ParameterName = "Opponent",
                    Value = opponent
                });
                command.Parameters.Add(new SqliteParameter
                {
                    ParameterName = "Datetime",
                    Value = datetime
                });
                var result = command.ExecuteNonQuery();
                Debug.Log(result);
            }
        }
    }


    public SqliteDataReader ExecuteReader(string connectionString, string commandText)
    {
        var dbConnection = new SqliteConnection(connectionString);

        using (var command = new SqliteCommand(commandText, dbConnection))
        {
            dbConnection.Open();
            SqliteDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            return reader;
        }
    }


    public void ResetDatabase()
    {
        PlayerPrefs.DeleteAll();
        File.Delete(Application.persistentDataPath + "/statsDatabase.db");
        Debug.Log("Izbrisano");
        SceneManager.LoadScene(0, LoadSceneMode.Single);
        CreateTables();
    }

    private string GenerateIdString(int size)
    {
        string allChars = "abcdefghijklmnopqrstuvwyxzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        char[] chars = new char[size];
        for (int i = 0; i < size; i++)
        {
            chars[i] = allChars[Random.Range(0, allChars.Length)];
        }
        return new string(chars);
    }
}
