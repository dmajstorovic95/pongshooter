﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class NameText : MonoBehaviour
{

    private TextMeshProUGUI text;

    void Start()
    {
        text = GetComponent<TextMeshProUGUI>();
        text.text = PlayerPrefs.GetString("Nickname");
    }

    private void OnEnable()
    {
        text.text = PlayerPrefs.GetString("Nickname");
    }
}
