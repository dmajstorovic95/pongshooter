﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityCoolDown : MonoBehaviour
{

    public string abilityButtonAxisName = "Fire1";
    public Image darkMask;
    public Text coolDownTextDisplay;
    public Transform origin;

    [SerializeField] private Ability ability;
    [SerializeField] private GameObject weaponHolder = null;
    private float coolDownDuration;
    private float nextReadyTime;
    private float coolDownTimeLeft;
    private bool coolDownComplete = false;

    void Start()
    {
        Initialize(ability, weaponHolder);
    }

    public void Initialize (Ability selectedAbility, GameObject weaponHolder)
    {
        ability = selectedAbility;
        coolDownDuration = ability.abilityCooldown;
        ability.Initialize(weaponHolder);
        AbilityReady();
    }
    void Update()
    {
        coolDownComplete = (Time.time > nextReadyTime);
        if (coolDownComplete)
        {
            AbilityReady();
            if (Input.GetKeyDown(abilityButtonAxisName))
            {
                ButtonTriggered();
            }
        }
        else
        {
            Cooldown();
        }
    }

    private void AbilityReady()
    {
        coolDownTextDisplay.enabled = false;
        darkMask.enabled = false;
    }

    private void Cooldown()
    {
        coolDownTimeLeft -= Time.deltaTime;
        float roundedCd = Mathf.Round(coolDownTimeLeft);
        coolDownTextDisplay.text = roundedCd.ToString();
        darkMask.fillAmount = (coolDownTimeLeft / coolDownDuration);
    }

    private void ButtonTriggered()
    {
        nextReadyTime = coolDownDuration + Time.time;
        coolDownTimeLeft = coolDownDuration;
        darkMask.enabled = true;
        coolDownTextDisplay.enabled = true;

        ability.TriggerAbility(origin);
    }

    public void TriggerByButton()
    {
        if (coolDownComplete)
        {
            ButtonTriggered();
        }
    }
}
