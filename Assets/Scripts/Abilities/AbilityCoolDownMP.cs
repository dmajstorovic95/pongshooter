﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Klasa za kontrolu aktiviranja specijalnih sposobnosti
public class AbilityCoolDownMP : MonoBehaviourPun
{

    public string abilityButtonAxisName = "Fire2";
    public Image darkMask;
    public Text coolDownTextDisplay;
    public Transform origin;

    [SerializeField] private Ability ability;
    [SerializeField] private GameObject weaponHolder = null;
    private float coolDownDuration;
    private float nextReadyTime;
    private float coolDownTimeLeft;
    private PhotonView PV;

    void Start()
    {
        PV = GetComponent<PhotonView>();
        Initialize(ability, weaponHolder);
    }

    public void Initialize (Ability selectedAbility, GameObject weaponHolder)
    {
        ability = selectedAbility;
        coolDownDuration = ability.abilityCooldown;
        ability.Initialize(weaponHolder);
        AbilityReady();
    }
    void Update()
    {
        bool coolDownComplete = (Time.time > nextReadyTime);
        if (coolDownComplete)
        {
            AbilityReady();

            if (PV.IsMine)
            {
                if (Input.GetButton(abilityButtonAxisName))
                {
                    PV.RPC("ButtonTriggered", RpcTarget.All);
                }
            }
        }
        else
        {
            Cooldown();
        }
    }

    private void AbilityReady()
    {
        coolDownTextDisplay.enabled = false;
        darkMask.enabled = false;
    }

    private void Cooldown()
    {
        coolDownTimeLeft -= Time.deltaTime;
        float roundedCd = Mathf.Round(coolDownTimeLeft);
        coolDownTextDisplay.text = roundedCd.ToString();
        darkMask.fillAmount = (coolDownTimeLeft / coolDownDuration);
    }

    [PunRPC]
    private void ButtonTriggered()
    {
        nextReadyTime = coolDownDuration + Time.time;
        coolDownTimeLeft = coolDownDuration;
        darkMask.enabled = true;
        coolDownTextDisplay.enabled = true;
 
        ability.TriggerAbility(origin);
    }
}
