﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName = "Abilities/ProjectileAbility")]
public class ProjectileAbility : Ability
{

    public GameObject projectile;

    private ProjectileShootTriggerable launcher;

    public override void Initialize(GameObject obj)
    {
        launcher = obj.GetComponent<ProjectileShootTriggerable>();
        launcher.projectile = projectile;
    }

    public override void TriggerAbility(Transform shotOrigin)
    {
        launcher.Launch(shotOrigin);
    }

}