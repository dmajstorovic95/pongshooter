﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Klasa za kontrolu razine zdravlja stita
public class ShieldController : MonoBehaviourPun, IPunObservable
{
    private int shieldPoints;
    private SpriteRenderer spriteRenderer;
    [HideInInspector] public GameObject Owner;

    void Start()
    {
        shieldPoints = Owner.GetComponent<ShieldAbilityTriggerable>().shieldPoints;
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void TakeDamage(int damage)
    {
        shieldPoints -= damage;
    }

    void Update()
    {
        if (gameObject != null)
        {
            if (shieldPoints <= 10)
            {
                spriteRenderer.color = Color.red;
            }
            if (shieldPoints <= 0)
            {
                Destroy(gameObject);
            }
        }
    }

    // Metoda iz Photon framework-a za sinkronizaciju razine zdravlja na mrezi
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(shieldPoints);
        }
        else
        {
            shieldPoints = (int)stream.ReceiveNext();
        }
    }
}
