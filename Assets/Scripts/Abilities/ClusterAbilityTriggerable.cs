﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

// Klasa za instanciranje sposobnosti rasprsenih projektila
public class ClusterAbilityTriggerable : MonoBehaviour
{

    [HideInInspector] public GameObject cluster;

    public void Launch(Transform shotOrigin)
    {
        for (int i = 0; i <= 4; i++)
        {
            GameObject clusterPiece = Instantiate(cluster, shotOrigin.position, shotOrigin.rotation * Quaternion.Euler(0, 0, i*(-2)));
            clusterPiece.GetComponent<Bullet>().Shooter = gameObject;
        }
    }
}
