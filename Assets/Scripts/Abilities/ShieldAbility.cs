﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Abilities/ShieldAbility")]
public class ShieldAbility : Ability
{
    public int shieldPoints = 50;
    public GameObject shield;

    private ShieldAbilityTriggerable activator;

    public override void Initialize(GameObject obj)
    {
        activator = obj.GetComponent<ShieldAbilityTriggerable>();
        activator.shieldPoints = shieldPoints;
        activator.shield = shield;
    }

    public override void TriggerAbility(Transform origin)
    {
        activator.Launch(origin);
    }
}
