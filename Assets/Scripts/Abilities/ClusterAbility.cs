﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Abilities/ClusterAbility")]
public class ClusterAbility : Ability
{

    public GameObject cluster;

    private ClusterAbilityTriggerable launcher;

    public override void Initialize(GameObject obj)
    {
        launcher = obj.GetComponent<ClusterAbilityTriggerable>();
        launcher.cluster = cluster;
    }

    public override void TriggerAbility(Transform shotOrigin)
    {
        launcher.Launch(shotOrigin);
    }
}
