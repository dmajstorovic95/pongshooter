﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Ability : ScriptableObject
{
    public string abilityName = "New Ability";
    public AudioClip abilitySound;
    public float abilityCooldown = 3f;

    public abstract void Initialize(GameObject gameObject);
    public abstract void TriggerAbility(Transform origin);
}
