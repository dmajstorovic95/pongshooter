﻿using UnityEngine;
using System.Collections;
using Photon.Pun;
using UnityEngine.SceneManagement;

// Klasa za instanciranje sposobnosti vatrene kugle
public class ProjectileShootTriggerable : MonoBehaviour
{

    [HideInInspector] public GameObject projectile;                            

    public void Launch(Transform shotOrigin)
    {
        GameObject clonedFireBall = Instantiate(projectile, shotOrigin.transform.position, shotOrigin.transform.rotation);
        clonedFireBall.GetComponent<Bullet>().Shooter = gameObject;
    }
}