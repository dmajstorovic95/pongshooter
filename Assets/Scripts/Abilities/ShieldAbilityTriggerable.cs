﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Klasa za instanciranje sposobnosti stita
public class ShieldAbilityTriggerable : MonoBehaviour
{
    [HideInInspector] public GameObject shield;
    [HideInInspector] public int shieldPoints = 50;

    public void Launch(Transform shieldSpawn)
    {
        GameObject abilityShield = Instantiate(shield, shieldSpawn.position, shieldSpawn.rotation);
        abilityShield.transform.parent = shieldSpawn.transform;
        abilityShield.GetComponent<ShieldController>().Owner = gameObject;
    }
}
