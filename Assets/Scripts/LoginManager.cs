﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoginManager : MonoBehaviour
{
    private void Start()
    {
        if (PlayerPrefs.HasKey("Nickname"))
        {
            DatabaseManager.Instance.Nickname = PlayerPrefs.GetString("Nickname");
            GameObject.FindGameObjectWithTag("GameMenu").transform.GetChild(1).gameObject.SetActive(true);
        }
        else
        {
            GameObject.FindGameObjectWithTag("GameMenu").transform.GetChild(5).gameObject.SetActive(true);
        }
    }
}
