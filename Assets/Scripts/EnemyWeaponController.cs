﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWeaponController : MonoBehaviour
{
    public GameObject shot;
    public Transform shotOrigin;
    public Transform target;
    public float fireRate;
    public float delay;

    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
        if (target != null)
        {
            InvokeRepeating("Fire", delay, fireRate);
        }
    }

    private void FixedUpdate()
    {
        if (target != null)
        {
            transform.up = target.position - transform.position;
        }
        else
        {
            CancelInvoke("Fire");
        }
    }

    void Fire()
    {
        Instantiate(shot, shotOrigin.position, shotOrigin.rotation);
    }

}
