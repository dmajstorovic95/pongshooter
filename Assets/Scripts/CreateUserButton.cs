﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateUserButton : MonoBehaviour
{
    public Button CustomButton;
    public GameObject welcome;
    public GameObject main;

    private string dbPath;
    void Awake()
    {
        dbPath = "Data Source=" + Application.persistentDataPath + "/statsDatabase.db";
        CustomButton.onClick.AddListener(CustomButton_onClick);
    }

    
    void CustomButton_onClick()
    {
        DatabaseManager.Instance.CreateUser(dbPath);
        welcome.gameObject.SetActive(false);
        main.gameObject.SetActive(true);
    }
}
