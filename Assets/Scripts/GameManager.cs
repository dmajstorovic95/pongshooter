﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            if (Instance != this)
            {
                Destroy(gameObject);
                Instance = this;
            }
        }
        //DontDestroyOnLoad(gameObject);
    }

    public void Quit()
    {
        StartCoroutine("MenuLoad");
    }

    public void Winner()
    {
        string opponent = PhotonRoomCustomMatch.room.Opponent;
        string winner = GameObject.FindGameObjectWithTag("Player").GetComponent<PhotonView>().Owner.NickName;
        if (winner == DatabaseManager.Instance.Nickname + " [ID: " + PlayerPrefs.GetString("Id") + "]")
        {
            DatabaseManager.Instance.UpdateUser(1, 50, opponent, "Data Source=" + Application.persistentDataPath + "/statsDatabase.db", System.DateTime.Now.ToString());
        }
        else
        {
            DatabaseManager.Instance.UpdateUser(0, 10, opponent, "Data Source=" + Application.persistentDataPath + "/statsDatabase.db", System.DateTime.Now.ToString());
        }
        transform.GetChild(0).GetChild(0).GetChild(1).gameObject.GetComponent<TextMeshProUGUI>().text = winner + "\n is the winner!";
    }

    IEnumerator MenuLoad()
    {
        yield return new WaitForSeconds(0.5f);
        if (SceneManager.GetActiveScene().name == "Multiplayer")
        {
            Destroy(PhotonRoomCustomMatch.room.gameObject);
            PhotonLobbyCustomMatch.lobby.FinishedBattle();
        }
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
}
