﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
    public float maxX, minX;
}

public class PlayerMove : MonoBehaviour
{
    public float speed;
    public Boundary boundary;

    private Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        Input.gyro.enabled = true;
    }

    void FixedUpdate()
    {
        Move();
        Rotate();
    }

    void Move()
    {
        float moveKeyboard = Input.GetAxis("Horizontal");
        Vector2 movementK = new Vector2(moveKeyboard, 0.0f);
        rb.velocity = movementK * speed;
        rb.position = new Vector2(Mathf.Clamp(rb.position.x, boundary.minX, boundary.maxX), rb.position.y);

        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                float target = 0.0f;
                Vector3 touchPosition = Camera.main.ScreenToWorldPoint(Input.touches[i].position);
                if (touchPosition.x < -1.5)
                {
                    target = -speed;
                }
                else if (touchPosition.x > 1.5)
                {
                    target = speed;
                }
                float moveTouch = Mathf.MoveTowards(rb.velocity.x, target, 10);
                Vector2 movement = new Vector2(moveTouch, 0.0f);
                rb.velocity = movement;
                rb.position = new Vector2(Mathf.Clamp(rb.position.x, boundary.minX, boundary.maxX), rb.position.y);
            }
        }
        else
        {
            rb.velocity = new Vector3(0, 0, 0);
        }
    }

    void Rotate()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.rotation = Quaternion.LookRotation(Vector3.forward, mousePos - transform.position);

        if (SystemInfo.supportsGyroscope)
        {
            Vector3 rot = transform.eulerAngles;
            rot.z = -Input.gyro.gravity.x * 100;
            transform.eulerAngles = rot;
        }

    }

}
