﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class PlayerController : MonoBehaviourPun, IPunObservable
{
    public float speed;
    public Boundary boundary;
    public GameObject shot;
    public Transform shotOrigin;
    public float fireRate;
    public int startingHealth;
    public Slider healthSlider;
    public GameObject explosion;

    private float nextFire;
    private PhotonView PV;
    private Rigidbody2D rb;
    private int currentHealth;


    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        PV = GetComponent<PhotonView>();
        Input.gyro.enabled = true;
        currentHealth = startingHealth;
    }

    void Update()
    {
        if (PV.IsMine)
        {
            Rotate();

            if (Input.touchCount > 0 && Time.time > nextFire)
            {
                for (int i = 0; i < Input.touchCount; i++)
                {
                    Vector3 touchPosition = Camera.main.ScreenToWorldPoint(Input.touches[i].position);
                    if (touchPosition.x > -1.5 && touchPosition.x < 1.5)
                    {
                        PV.RPC("RPC_Fire", RpcTarget.All);
                    }
                }
            }
        }
    }

    void FixedUpdate()
    {
        if (PV.IsMine)
        {
            Move();
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(currentHealth);
        }
        else
        {
            currentHealth = (int)stream.ReceiveNext();
        }
    }


    [PunRPC]
    void RPC_Fire()
    {
        nextFire = Time.time + fireRate;
        GameObject bullet = Instantiate(shot, shotOrigin.position, shotOrigin.rotation);
        bullet.GetComponent<Bullet>().Shooter = gameObject;
    }

    void Rotate()
    {
        //Mis
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.rotation = Quaternion.LookRotation(Vector3.forward, mousePos - transform.position);

        //Ziroskop
        if (SystemInfo.supportsGyroscope)
        {
            Vector3 rot = transform.eulerAngles;
            if (transform.position.y == 4)
                rot.z = -Input.gyro.gravity.x * 100 + 180;
            else rot.z = -Input.gyro.gravity.x * 100;
            transform.eulerAngles = rot;
        }
    }

    void Move()
    {
        //Tipkovnica
        float moveHorizontal = Input.GetAxis("Horizontal");
        Vector2 movement = new Vector2(moveHorizontal, 0.0f);
        if (transform.position.y == 4)
        {
            rb.velocity = movement * speed * (-1);
        }
        else
        {
            rb.velocity = movement * speed;

        }
        rb.position = new Vector2(Mathf.Clamp(rb.position.x, boundary.minX, boundary.maxX), rb.position.y);

        //Touch
        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                float target = 0.0f;
                Vector3 touchPosition = Camera.main.ScreenToWorldPoint(Input.touches[i].position);
                if (touchPosition.x < -1.5)
                {
                    target = -speed;
                }
                else if (touchPosition.x > 1.5)
                {
                    target = speed;
                }
                float moveTouch = Mathf.MoveTowards(rb.velocity.x, target, 10);
                Vector2 movementT = new Vector2(moveTouch, 0.0f);
                rb.velocity = movementT;
                rb.position = new Vector2(Mathf.Clamp(rb.position.x, boundary.minX, boundary.maxX), rb.position.y);
            }
        }
        else
        {
            rb.velocity = new Vector3(0, 0, 0);
        }
    }


    
    public void TakeDamage(int damage)
    {
        currentHealth -= damage;

        healthSlider.value = currentHealth;

        if (currentHealth <= 0)
        {
            Instantiate(explosion, transform.position, transform.rotation);
            PhotonNetwork.Destroy(gameObject);
            //Destroy(gameObject);
        }
    }

    private void OnDestroy()
    {
        if (GameManager.Instance != null)
        {
            //GameManager.Instance.StartCoroutine("MenuLoad");
            GameManager.Instance.Winner();
            GameManager.Instance.transform.GetChild(0).gameObject.SetActive(true);
        }
    }

}
