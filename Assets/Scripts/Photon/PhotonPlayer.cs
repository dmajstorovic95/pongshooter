﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class PhotonPlayer : MonoBehaviour
{
    public GameObject myCharacter;

    private PhotonView PV;
    int spawnPicker;

    // Start is called before the first frame update
    void Start()
    {
        PV = GetComponent<PhotonView>();


        if (PhotonNetwork.IsMasterClient)
        {
             spawnPicker = 0;
        } else
        {
             spawnPicker = 1;
        }
        if (PV.IsMine)
        {
            myCharacter = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", PlayerInfo.PI.allCharacters[PlayerInfo.PI.mySelectedCharacter].name), GameSetup.GS.spawnPoints[spawnPicker].position, GameSetup.GS.spawnPoints[spawnPicker].rotation, 0);
        }
    }
}