﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PhotonLobbyCustomMatch : MonoBehaviourPunCallbacks, ILobbyCallbacks 
{
    public static PhotonLobbyCustomMatch lobby;
    [HideInInspector]public string roomName;
    public GameObject roomListingPrefab;
    public Transform roomsPanel;
    public GameObject offlineButton;
    public GameObject roomsButton;

    private void Awake()
    {
        lobby = this; 
    }

    public void StartMP()
    {
        PhotonNetwork.ConnectUsingSettings();
    }

    public void OnBackButtonClicked()
    {
        Debug.Log("Disconnected");
        PhotonNetwork.Disconnect();
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("Player has connected to photon master server");
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.NickName = DatabaseManager.Instance.Nickname + " [ID: " + PlayerPrefs.GetString("Id") + "]";
        roomsButton.SetActive(true);
        offlineButton.SetActive(false);
        Debug.Log(PhotonNetwork.CloudRegion);
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        base.OnRoomListUpdate(roomList);
        RemoveRoomListings();
        foreach (RoomInfo room in roomList)
        {
            ListRoom(room);
        }
    }

    void RemoveRoomListings()
    {
        int i = 0;
        while (roomsPanel.childCount != 0)
        {
            Destroy(roomsPanel.GetChild(i).gameObject);
            i++;
        }
    }

    void ListRoom(RoomInfo room)
    {
        if (room.IsOpen && room.IsVisible)
        {
            GameObject tempListing = Instantiate(roomListingPrefab, roomsPanel);
            RoomButton tempButton = tempListing.GetComponent<RoomButton>();
            tempButton.roomName = room.Name;
            tempButton.SetRoom();
        }
    }



    public void CreateRoom()
    {
        Debug.Log("Trying to create a new room");
        RoomOptions roomOps = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = 2 };
        PhotonNetwork.CreateRoom(roomName, roomOps);
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("We are now in the room");
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("Tried to create a room but failed, there must be a room with the same name");
        //CreateRoom();
    }

    public void OnRoomNameChanged(string name)
    {
        roomName = name;
    }


    public void JoinLobbyOnClick()
    {
        if (!PhotonNetwork.InLobby)
        {
            PhotonNetwork.JoinLobby();
        }
    }

    public void FinishedBattle()
    {
        PhotonNetwork.Disconnect();
    }
}
