﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Bullet : MonoBehaviour
{
    Rigidbody2D rb;
    public float speed;
    public int damage;
    public GameObject explosion;

    public GameObject Shooter { get; set; }

    private PhotonView PV; 

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = transform.up * speed;
        PV = GetComponent<PhotonView>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("BounceWall"))
        {
            Vector2 wallNormal = collision.contacts[0].normal;
            Vector2 refDirection = Vector2.Reflect(rb.velocity, wallNormal);

            rb.velocity = refDirection;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Instantiate(explosion, transform.position, transform.rotation);
            if (SceneManager.GetActiveScene().name == "Multiplayer")
            {  
                collision.gameObject.GetComponent<PlayerController>().TakeDamage(damage);
                Destroy(gameObject);
            }
            else if (SceneManager.GetActiveScene().name == "Training")
            {
                collision.gameObject.GetComponent<PlayerHealth>().TakeDamage(damage);
                Destroy(gameObject);
            }
        }
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Instantiate(explosion, transform.position, transform.rotation);
            collision.gameObject.GetComponent<EnemyHealth>().TakeDamage(damage);
            Destroy(gameObject);
        }
        if (collision.gameObject.CompareTag("Shield"))
        {
            if (Shooter != collision.gameObject.transform.parent.gameObject)
            {
                Instantiate(explosion, transform.position, transform.rotation);
                collision.gameObject.GetComponent<ShieldController>().TakeDamage(damage);
                Destroy(gameObject);
            }
        }
    }
}
