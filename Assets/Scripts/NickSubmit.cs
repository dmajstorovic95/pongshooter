﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NickSubmit : MonoBehaviour
{
    void Start()
    {
        var input = gameObject.GetComponent<InputField>();
        var se = new InputField.OnChangeEvent();
        se.AddListener(SubmitNick);
        input.onValueChanged = se;
    }

    private void SubmitNick(string nick)
    {
        DatabaseManager.Instance.Nickname = nick;
    }
}
