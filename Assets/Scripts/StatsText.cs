﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class StatsText : MonoBehaviour
{
    private TextMeshProUGUI textMesh;
    // Start is called before the first frame update
    void Start()
    {
        textMesh = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        GameObject chr = PlayerInfo.PI.allCharacters[PlayerInfo.PI.mySelectedCharacter];
        string sp = "";
        switch (PlayerInfo.PI.mySelectedCharacter) {
            case 0:
                sp = "SHIELD";
                break;
            case 1:
                sp = "FIREBALL";
                break;
            case 2:
                sp = "CLUSTER";
                break;
        }
        textMesh.text = "HEALTH: " + chr.GetComponent<PlayerController>().startingHealth.ToString() +
                        "\n\nSPEED: " + chr.GetComponent<PlayerController>().speed.ToString() +
                        "\n\nSPECIAL ABILITY: " + sp;
    }
}
