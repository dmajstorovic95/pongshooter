﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClearPrefs : MonoBehaviour
{
    public Button button;
    void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(Task);
    }

    void Task()
    {
        PlayerPrefs.DeleteAll();
    }
}
