﻿using UnityEngine;
using System.Collections.Generic;

public class CameraResolution : MonoBehaviour
{
    void Start()
    {
        RescaleCamera();
    }


    void Update()
    {
        RescaleCamera();
    }

    private void RescaleCamera()
    {

        float targetAspect = 9.0f / 16.0f;
        float windowAspect = (float)Screen.width / (float)Screen.height;
        float scaleHeight = windowAspect / targetAspect;
        Camera camera = GetComponent<Camera>();

        if (scaleHeight < 1.0f)
        {
            Rect rect = camera.rect;

            rect.width = 1.0f;
            rect.height = scaleHeight;
            rect.x = 0;
            rect.y = (1.0f - scaleHeight) / 2.0f;

            camera.rect = rect;
        }
        else
        {
            float scaleWidth = 1.0f / scaleHeight;

            Rect rect = camera.rect;

            rect.width = scaleWidth;
            rect.height = 1.0f;
            rect.x = (1.0f - scaleWidth) / 2.0f;
            rect.y = 0;

            camera.rect = rect;
        }
    }
   

    void OnPreCull()
    {
        Rect wp = Camera.main.rect;
        Rect nr = new Rect(0, 0, 1, 1);
        Camera.main.rect = nr;
        GL.Clear(true, true, Color.black);
        Camera.main.rect = wp;
    }
}

